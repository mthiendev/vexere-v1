const express = require('express');
const {
  register,
  login,
  uploadAvatar,
  getAllTrip,
} = require('../controller/user.controllers');
const { authenticate } = require('../middlewares/auth/authenticate');
const { uploadImage } = require('../middlewares/upload/uploadImages');

const userRouter = express.Router();

userRouter.post('/register', register);
userRouter.post('/login', login);
//upload file
userRouter.post(
  '/upload-avatar',
  authenticate,
  uploadImage('avatar'),
  uploadAvatar,
);
userRouter.get('/all-trip', getAllTrip);

module.exports = {
  userRouter,
};
