const express = require('express');
const { stationRouter } = require('./station.router');
const { fingerPrintRouter } = require('./test-finger-print');
const { tripRouter } = require('./trip.router');
const { userRouter } = require('./usere.router');
const rootRouter = express.Router();

rootRouter.use('/stations', stationRouter);
rootRouter.use('/users', userRouter);
rootRouter.use('/trips', tripRouter);
rootRouter.use('/test-finger-print', fingerPrintRouter);
module.exports = {
  rootRouter,
};
