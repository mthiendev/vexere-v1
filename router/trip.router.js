const express = require('express');
const {
  createTrip,
  getAllTrip,
  deleteTrip,
  updateTrip,
} = require('../controller/trip.controller');
const { authenticate } = require('../middlewares/auth/authenticate');
const { authorize } = require('../middlewares/auth/authorize');
const tripRouter = express.Router();
tripRouter.post('/', authenticate, createTrip);
tripRouter.get('/', getAllTrip);
tripRouter.delete(
  '/:id',
  // authenticate,
  // authorize(['admin', 'super_admin']),
  deleteTrip,
);
tripRouter.put('/:id', updateTrip);
module.exports = {
  tripRouter,
};
