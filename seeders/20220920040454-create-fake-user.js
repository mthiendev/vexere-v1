'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     *
     */
    await queryInterface.bulkInsert(
      'Users',
      [
        {
          name: 'Chi Trung',
          email: 'trung@gmail.com',
          password: '123456',
          numberPhone: '123456789',
          type: 'admin',
          avatar: '',
          createdAt: '2022-09-19 10:11:20',
          updatedAt: '2022-09-19 17:20:21',
        },
        {
          name: 'Nhất Tín',
          email: 'tin@gmail.com',
          password: '123456',
          numberPhone: '123456789',
          type: 'admin',
          avatar: '',
          createdAt: '2022-09-19 10:11:20',
          updatedAt: '2022-09-19 17:20:21',
        },
        {
          name: 'Đăng Khoa',
          email: 'khoa@gmail.com',
          password: '123456',
          numberPhone: '123456789',
          type: 'admin',
          avatar: '',
          createdAt: '2022-09-19 10:11:20',
          updatedAt: '2022-09-19 17:20:21',
        },
      ],
      {},
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null, {});
  },
};
