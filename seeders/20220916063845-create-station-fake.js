'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      'stations',
      [
        {
          name: 'Bến xe phía Nam Nha Trang',
          address: 'Vĩnh Trung, Nha Trang',
          province: 'Nha Trang',
          createdAt: '2022-09-14 15:38:03',
          updatedAt: '2022-09-14 15:38:03',
        },
        {
          name: 'Bến Xe Thành Bưởi, Hàng Xanh',
          address: '630 Điện Biên Phủ, Phường 22, Bình Thạnh',
          province: 'Hồ Chí Minh',
          createdAt: '2022-09-14 16:43:32',
          updatedAt: '2022-09-14 16:43:32',
        },
        {
          name: 'Bến xe Liên tỉnh Đà Lạt',
          address: '1 Đường Tô Hiến Thành, Phường 3, Thành phố Đà Lạt',
          province: 'Lâm Đồng',
          createdAt: '2022-09-14 16:43:32',
          updatedAt: '2022-09-14 16:43:32',
        },
        {
          name: 'Bến Xe Miền Tây',
          address: '395 Kinh Đ. Vương, An Lạc, Bình Tân',
          province: 'Hồ Chí Minh',
          createdAt: '2022-09-14 16:43:32',
          updatedAt: '2022-09-14 16:43:32',
        },
        {
          name: 'Bến Xe An Sương',
          address: 'QL22, Bà Điểm, Hóc Môn',
          province: 'Hồ Chí Minh',
          createdAt: '2022-09-14 16:43:32',
          updatedAt: '2022-09-14 16:43:32',
        },
      ],
      {},
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('stations', null, {});
  },
};
