'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      'trips',
      [
        {
          fromStation: 40,
          toStation: 41,
          startTime: '2022-9-21 08:30:00',
          price: 200000,
          createdAt: '2022-09-19 10:11:20',
          updatedAt: '2022-09-19 17:20:21',
        },
        {
          fromStation: 39,
          toStation: 43,
          startTime: '2022-9-21 08:30:00',
          price: 200000,
          createdAt: '2022-09-19 10:11:20',
          updatedAt: '2022-09-19 17:20:21',
        },
        {
          fromStation: 41,
          toStation: 42,
          startTime: '2022-9-21 08:30:00',
          price: 150000,
          createdAt: '2022-09-19 10:11:20',
          updatedAt: '2022-09-19 17:20:21',
        },
      ],
      {},
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('trips', null, {});
  },
};
