'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      'Tickets',
      [
        {
          trip_id: 16,
          user_id: 12,
          createdAt: '2022-09-19 10:11:20',
          updatedAt: '2022-09-19 17:20:21',
        },
        {
          trip_id: 17,
          user_id: 13,
          createdAt: '2022-09-19 10:11:20',
          updatedAt: '2022-09-19 17:20:21',
        },
        {
          trip_id: 18,
          user_id: 14,
          createdAt: '2022-09-19 10:11:20',
          updatedAt: '2022-09-19 17:20:21',
        },
      ],
      {},
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Tickets', null, {});
  },
};
