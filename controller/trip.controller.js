const { where } = require('sequelize');
const { Trip, Station } = require('../models');
const createTrip = async (req, res) => {
  const { fromStation, toStation, startTime, price } = req.body;
  const newTrip = await Trip.create({
    fromStation,
    toStation,
    startTime,
    price,
  });
  res.status(201).send(newTrip);
};
const getAllTrip = async (req, res) => {
  const tripList = await Trip.findAll({
    include: [
      {
        model: Station,
        as: 'from',
      },
      {
        model: Station,
        as: 'to',
      },
    ],
  });
  res.status(200).send(tripList);
};
const deleteTrip = async (req, res) => {
  const { id } = req.params;
  try {
    await Trip.destroy({
      where: {
        id,
      },
    });
    res.status(200).send(`Đã xóa trip có id là : ${id}`);
  } catch (error) {
    res.status(500).send(error);
  }
};
const updateTrip = async (req, res) => {
  const { id } = req.params;
  const { fromStation, toStation, startTime, price } = req.body;
  try {
    const detailTrip = await Trip.findOne({
      where: {
        id,
      },
    });
    if (detailTrip) {
      detailTrip.fromStation = fromStation;
      detailTrip.toStation = toStation;
      detailTrip.startTime = startTime;
      detailTrip.price = stapricertTime;
      await detailTrip.save();
      res.status(200).send(detailTrip);
    } else {
      res.status(404).send('not found');
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
module.exports = {
  createTrip,
  getAllTrip,
  deleteTrip,
  updateTrip,
};
