const { User, sequelize } = require('../models');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

const gravatarUrl = require('gravatar-url');
const register = async (req, res) => {
  const { name, email, password, numberPhone } = req.body;

  try {
    // Tạo avatar mặc định
    const avatar = gravatarUrl(email);
    //tạo ra 1 chuỗi ngẫu nhiên
    const salt = bcryptjs.genSaltSync(10);
    //ma hóa chuỗi ngẫu nhiên salt +password
    const hashPassword = bcryptjs.hashSync(password, salt);
    const newUser = await User.create({
      name,
      email,
      password: hashPassword,
      numberPhone,
      avatar,
    });
    res.status(200).send(newUser);
  } catch (error) {
    res.status(500).send(error);
  }
};
const login = async (req, res) => {
  const { email, password } = req.body;
  //b1 tìm ra user đang đăng nhập dựa trên email
  const user = await User.findOne({
    where: { email },
  });
  //b2 kiểm tra mật khẩu có đúng hay ko
  if (user) {
    const isAuth = bcryptjs.compareSync(password, user.password);
    if (isAuth) {
      const token = jwt.sign(
        { email: user.email, type: user.type },
        'thanh-nhan-2211',
        { expiresIn: 60 * 60 },
      );
      res.status(200).send({ message: 'Đăng nhập thành công!', token });
    } else {
      res.status(500).send({ message: 'Tài khoản hoặc mật khẩu không đúng' });
    }
  } else {
    res.status(404).send({ message: 'Không tìm thấy email phù hợp' });
  }
};
const uploadAvatar = async (req, res) => {
  const { file } = req;
  const urlImage = `http://localhost:3000/${file.path}`;
  const { user } = req;
  const userFound = await User.findOne({
    email: user.email,
  });
  userFound.avatar = urlImage;
  await userFound.save();

  res.send(userFound);
};
const getAllTrip = async (req, res) => {
  try {
    const [results, metadata] =
      await sequelize.query(`select users.name as userName,fromSta.name as fromStation,toSta.name as toStation,trips.startTime,trips.price from users
    inner join tickets on users.id = tickets.user_id
    inner join trips on trips.id = tickets.trip_id
    inner join stations as fromSta on fromSta.id = trips.fromStation
    inner join stations as toSta on toSta.id = trips.toStation`);
    res.status(200).send(results);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = {
  register,
  login,
  uploadAvatar,
  getAllTrip,
};
