'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Station extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Trip }) {
      // define association here
      this.hasMany(Trip, { foreignKey: 'fromStation', as: 'from' });
      this.hasMany(Trip, { foreignKey: 'toStation', as: 'to' });
    }
  }
  Station.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          len: [3, 30],
          notEmpty: true,
        },
      },
      address: {
        type: DataTypes.STRING,
        validate: {
          checkLen(value) {
            if (value.length > 5) {
              return true;
            } else {
              throw new Error('Độ dài phải lớn hơn 5');
            }
          },
        },
      },
      province: {
        type: DataTypes.STRING,
        validate: {
          isIn: [
            [
              'An Giang',
              'Bà Rịa-Vũng Tàu',
              'Bạc Liêu',
              'Bắc Kạn',
              'Bắc Giang',
              'Bắc Ninh',
              'Bến Tre',
              'Bình Dương',
              ' Bình Định',
              'Bình Phước',
              'Bình Thuận',
              'Cà Mau',
              'Cao Bằng',
              'Cần Thơ',
              'Đà Nẵng',
              'Đắk Lắk',
              'Đắk Nông',
              'Điện Biên',
              'Đồng Nai',
              'Đồng Tháp',
              'Gia Lai',
              'Hà Giang',
              'Hà Nam',
              'Hà Nội',
              'Hà Tây',
              'Hà Tĩnh',
              'Hải Dương',
              'Hải Phòng',
              'Hòa Bình',
              'Hồ Chí Minh',
              'Hậu Giang',
              'Hưng Yên',
              'Khánh Hòa',
              'Kiên Giang',
              'Kon Tum',
              'Lai Châu',
              'Lào Cai',
              'Lạng Sơn',
              'Lâm Đồng',
              'Long An',
              'Nam Định',
              'Nghệ An',
              'Ninh Bình',
              'Ninh Thuận',
              'Phú Thọ',
              'Phú Yên',
              'Quảng Bình',
              'Quảng Nam',
              'Quảng Ngãi',
              'Quảng Ninh',
              'Quảng Trị',
              'Sóc Trăng',
              'Sơn La',
              'Tây Ninh',
              'Thái Bình',
              'Thái Nguyên',
              'Thanh Hóa',
              'Thừa Thiên – Huế',
              'Tiền Giang',
              'Trà Vinh',
              'Tuyên Quang',
              'Vĩnh Long',
              'Vĩnh Phúc',
              'Yên Bái',
            ],
          ],
        },
      },
    },
    {
      sequelize,
      modelName: 'Station',
    },
  );
  return Station;
};
