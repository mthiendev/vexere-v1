const checkExist = (Model) => {
  return async (req, res, next) => {
    // kiểm tr staton có tồn tại ko
    const { id } = req.params;
    const station = await Model.findOne({
      where: {
        id,
      },
    });
    if (station) {
      next();
    } else {
      res.status(404).send(`Không tìm thấy Id là ${id}`);
    }
  };
};
module.exports = {
  checkExist,
};
