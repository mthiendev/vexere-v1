const multer = require('multer');
const mkdirp = require('mkdirp');

const uploadImage = (type) => {
  const made = mkdirp.sync(`./public/images/${type}`);
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `./public/images/${type}`); //setup chỗ cần lưu file // null là !error
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '_' + file.originalname); // đặt lại tên cho file
    },
  });
  const upload = multer({
    storage,
    fileFilter: function (req, file, cb) {
      const extensionImageList = ['.png', '.jpg', 'JPEG', 'GIF', 'SVG'];
      const extension = file.originalname.slice(-4);
      const check = extensionImageList.includes(extension);
      if (check) {
        cb(null, true);
      } else {
        cb(new Error('extension không hợp lệ'));
      }
    },
  });
  return upload.single('avatar');
};
module.exports = { uploadImage };
