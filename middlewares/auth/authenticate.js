const jwt = require('jsonwebtoken');
const authenticate = (req, res, next) => {
  try {
    const token = req.header('token');
    const decode = jwt.verify(token, 'thanh-nhan-2211');

    if (decode) {
      req.user = decode;
      return next();
    } else {
      res.status(401).send('Bạn chưa đăng nhập');
    }
  } catch (error) {
    res.status(401).send('Bạn chưa đăng nhập');
  }
};
module.exports = {
  authenticate,
};
